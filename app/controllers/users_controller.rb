#Листинг 7.5: Контроллер Users с действием show. app/controllers/users_controller.rb Листинг 7.6: Контроллер Users с отладчиком. app/controllers/users_controller.rb  Листинг 7.12: Добавление переменной @user к действию new. app/controllers/users_controller.rb Листинг 7.16: Действие create, которое может обрабатывать провальную регистрацию. app/controllers/users_controller.rb Листинг 7.17: Использование строгих параметров в действии create. app/controllers/users_controller.rb Листинг 7.23: Действие create с сохранением и перенаправлением. app/controllers/users_controller.rb Листинг 7.24: Добавление флэш-сообщения к регистрации пользователя. app/controllers/users_controller.rb Листинг 8.22: Вход пользователя после регистрации. app/controllers/users_controller.rb Листинг 9.25: Окончательный предфильтр correct_user. ЗЕЛЕНЫЙ app/controllers/users_controller.rb Листинг 9.28: Добавление store_location в предфильтр logged-in . app/controllers/users_controller.rb Листинг 9.32: Требование входа пользователя для действия index. ЗЕЛЕНЫЙ app/controllers/users_controller.rbЛистинг 9.33: Действие index. app/controllers/users_controller.rbЛистинг 9.53: Добавление работающего действия destroy. app/controllers/users_controller.rbЛистинг 10.34: Отправление электронного письма через объект модели User. app/controllers/users_controller.rb    

class UsersController < ApplicationController
before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :correct_user,   only: [:edit, :update]

  def index
 @users = User.paginate(page: params[:page])
  end
  
def show
    @user = User.find(params[:id])
  end

  def new
       @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      #log_in @user
 @user.send_activation_email
           flash[:success] = "Welcome to the Sample App!"
           redirect_to @user
    else
      render 'new'
    end
  end
def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

def edit
  end
              private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
# Предфильтры

    # Подтверждает вход пользователя
    def logged_in_user
      unless logged_in?
store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Подтверждает правильного пользователя
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
 # Подтверждает администратора.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
